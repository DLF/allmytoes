# AllMyToes
A program that provides thumbnails for images by using the thumbnail data-base
specified by [freedesktop.org](https://specifications.freedesktop.org/thumbnail-spec/thumbnail-spec-latest.html)
(aka XDG standard).

This project has moved to [https://gitlab.com/allmytoes/allmytoes](https://gitlab.com/allmytoes/allmytoes).
